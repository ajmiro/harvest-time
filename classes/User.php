<?php 

class User{

	private static $_instance = null; 
	private $_results, 
					$_data,
					$_sessionName, 
					$_api,
					$_isLoggedIn = false,
					$_userId; 

	public function __construct( $user = null ){

		require_once getcwd() . '/HaPi/Harvest/User.php';

		$this->_api = new HarvestAPI(); 
		
		$this->_sessionName = Config::get('session/session_name');

		if( !$user ) {

			if(Session::exists($this->_sessionName)){

				$user = Session::get($this->_sessionName);
				$this->_isLoggedIn = true;

				if($this->getUser( $user )){

				} else{
					//process logout
				}
			} 

		} else {

			$this->_api->setUser( Session::get(Config::get('session/ht_email')) ); 
			$this->_api->setPassword( Session::get(Config::get('session/ht_pass') ) ); 
			$this->_api->setAccount( Config::get('session/ht_account') );
			$this->_api->setRetryMode( HarvestAPI::RETRY );

			//null user, then create new user
			$this->getUser( $user );
			$this->_userId = $user;

		}

	
	}

	public function login( $username = null, $password = null ){

		//Set Credentials
		$this->_api->setUser( $username ); 
		$this->_api->setPassword( $password ); 
		$this->_api->setAccount( Config::get('session/ht_account') );
		$this->_api->setRetryMode( HarvestAPI::RETRY );

		$results = $this->_api->getWhoIAm();

		if( $results->isSuccess() ){
			
			$this->_data = $results->data->user;
			$user = explode(PHP_EOL, $this->_data);
			$userId = $user[4];

			Session::put(Config::get('session/ht_email'), $username);
			Session::put(Config::get('session/ht_pass'), $password);
			Session::put($this->_sessionName, $userId);
		
			return true;
		}

		return false; 

	}

	public function getUser( $userId ){

		$results = $this->_api->getUser( intval($userId) );
		
		if($results->isSuccess() ){

			$this->_data = $results->data;
			return true;
		}

		return 	false;
	}

	public function getUsers(){

		$results = $this->_api->getUsers();
		
		if($results->isSuccess() ){
			$this->_data = $results->data;
			return $results->data;
		}

		return false;
	}

	public function data(){
		return $this->_data;
	}

	public function get( $field = null ){

		$dataArr = [];
		$fieldSize = count( $field );

		for( $i = 0; $i < $fieldSize; $i++ ){

			$dataArr["'" . $field[$i] . "'"] = $this->_data->{$field[$i]};

		}
		return $dataArr;
	}

	public function resetPassword(){

		$result = $this->_api->resetUserPassword( $this->_userId );
		if( $result->isSuccess() ) {
			Session::flash('msg', '<div class="alert alert-success">Reset password instruction is sent to your email.</div>');
		} else{
			Session::flash('msg', '<div class="alert alert-danger">Please try again</div>');
		}

		Redirect::to('index');

	}

	public function update( $fields = null ){

		$user = new Harvest_User();
		$user->set( 'id', $this->_userId );

		foreach( $fields as $key => $value ){
			$user->set( "{$key}", $value );
		}

		$result = $this->_api->updateUser( $user );
		
		if( $result->isSuccess() ) {
			Session::flash('msg', '<div class="alert alert-success">Profile successfully saved.</div>');
			Redirect::to('index');
		}else{
			Session::flash('msg', '<div class="alert alert-danger">Something is wrong. Please check</div>');
			return false;
		}
		
	}

	public function getUserEntries(){

		require_once getcwd() . '/HaPi/Harvest/Range.php';

		$range = new Harvest_Range( date('Ymd'), date('Ymd') );

		$this->_api = new HarvestAPI();

		$result = $this->_api->getUserEntries( $this->_userId, $range );
		if( $result->isSuccess() ) {
		   $dayEntries = $result->data;
		   print_r( $dayEntries ); 
		}


	}

	public function isLoggedIn(){
		return $this->_isLoggedIn;
	}

	public function logout(){
		$currentSession = Config::get('session/session_name');
		if(Session::exists($currentSession)){
			Session::delete($currentSession);
			Redirect::to('login');
		}

	}


}