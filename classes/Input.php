<?php 

class Input{

	public static function exists( $type = 'post'){
		switch ($type) {
			case 'post':
				return ( !empty($_POST)) ? true : false;
			break;
			case 'get':
				return ( !empty($_GET)) ? true : false;
				break;
			
			default:
				return false;
				break;
		}
	}

	public static function get( $name ){
		if( isset( $_POST[$name]) ){
			return $_POST[$name];

		}else if( $_GET[$name]){
			return $_GET[$name];
		}

		return '';
	}

	public static function check( $type = 'post', $name ){
		switch ('post') {
			case 'post':
				return (isset( $_POST[$name]) ) ? true : false;
				break;
			
			default:
				break;
		}
	}

	public static function resetInput($type = 'post'){
		switch ($type) {
			case 'post':
				unset($_POST);
				break;

			case 'get':
				unset($_GET);
				break;
			
			default:
				return false;
				break;
		}
	}

}