<?php 

class Project{

	private $_project,
					$_api,
					$_data,
					$_tasks,
					$_teamMembers,
					$_errors;


	public function __construct( $project = null ){

		if(Session::exists(Config::get('session/session_name'))) {

			require_once getcwd() . '/HaPi/Harvest/Project.php';
			require_once getcwd() . '/HaPi/Harvest/Task.php';

			$this->_api = new HarvestAPI(); 
			
			$this->_api->setUser( Session::get(Config::get('session/ht_email')) ); 
			$this->_api->setPassword( Session::get(Config::get('session/ht_pass') ) ); 
			$this->_api->setAccount( Config::get('session/ht_account') );

			$this->_api->setRetryMode( HarvestAPI::RETRY );

			if( !$project ) {

			} else {

				$this->getProject( $project );
				$this->getProjectTaskAssignments( $project );
				$this->getProjectUserAssignments( $project );
			
			}

		} else {

			Redirect::to('login');

		}

		

	}

	public function getProjects(){
	
		$results = $this->_api->getProjects();

		if( $results->isSuccess() ){
			$this->_data = $results->data;

			return $this->_data;
		}

	}

	public function getProject( $projectId = null ){

		$results = $this->_api->getProject( $projectId );

		if( $results->isSuccess() ){

			$this->_project = $results->data;
			return true;
		}

		return false; 
	}

	public function create( $fields = null ){

	 	$project = new Harvest_Project();
	 	$tasks = array();
	 	
		foreach($fields as $key => $value ){

			if( $key !== 'tasks'){
				$project->set( "{$key}", $value );
			} else{
				$tasks = $fields[$key];
			}
		 	
		} 
		 
		$result = $this->_api->createProject( $project );
		
		if( $result->isSuccess() ) {

			$projectId = $result->data;

			if( !empty( $tasks ) ){

				return ($this->createProjectTaskAssignment( $tasks, $projectId )) ? true : false;
			
			} else{
				return true;
			}
					
		} else{ 
			
			$this->_errors = $results->data;
			return false;
		}

		//return false;

	}

	private function createProjectTaskAssignment( $tasks = array(), $projectId ) {

		foreach( $tasks as $task ){	

			$taskObj = new Task( $task  );
			$newTaskAssignment = $taskObj->data();
			$resultTaskAssignment = $this->_api->createProjectTaskAssignment( $projectId, $newTaskAssignment );

			if( $resultTaskAssignment->isSuccess() ){
				
				return true;

			} 

			return false;
			
		}

	}

	public function deleteProject( $projectId = null ){

		$results = $this->_api->deleteProject( $projectId );

		if( $results->isSuccess() ){ 
			Session::flash('msg', '<div class="alert alert-success">Project successfully deleted.</div>');
		} else{
			Session::flash('msg', '<div class="alert alert-danger">Project does not exists.</div>');
		}

		Redirect::to('project');
	}

	public function updateProject( $project_id = null ){

		
	}

	public function getProjectTaskAssignments( $project = null ){

		$results = $this->_api->getProjectTaskAssignments( $project );

		if( $results->isSuccess() ){
			$this->_tasks = $results->data;

			return $this->_tasks;
		}

	}

	public function getProjectUserAssignments( $project = null ){

		$results = $this->_api->getProjectUserAssignments( $project );

		if( $results->isSuccess() ){
			$this->_teamMembers = $results->data;

			return $this->_teamMembers;
		}

	}

	public function get(){
		return $this->_project;
	}

	public function getTasks(){
		return $this->_tasks;
	}

	public function getTeamMembers(){
		return $this->_teamMembers;
	}

	public function getErrors(){
		return $this->_errors;
	}

}



	

	








