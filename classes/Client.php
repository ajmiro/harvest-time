<?php

class Client{
	
	private $_client, $_data; 

	public function __construct( $client = null ){

		if(Session::exists(Config::get('session/session_name'))){

			$this->_api = new HarvestAPI();
			$this->_api->setUser( Session::get(Config::get('session/ht_email')) ); 
			$this->_api->setPassword( Session::get(Config::get('session/ht_pass') ) ); 
			$this->_api->setAccount( Config::get('session/ht_account') );
			$this->_api->setRetryMode( HarvestAPI::RETRY );

			if( !$client ) {

			} else {

				$this->getClient( $client );
			
			}

		} else{
			Redirect::to('login');
		}

	}

	public function getClients(){

		$results = $this->_api->getClients();

		if( $results->isSuccess() ){
			$this->_data = $results->data;
			
			return $this->_data;
		}	

		return false;

	}

	public function getClient( $client = null ){
		
		$results = $this->_api->getClient( $client );

		if( $results->isSuccess() ){
			$this->_client = $results->data;
			return true;
		}		

		return false;

	}

	public function get(){

		return $this->_client;

	}

}