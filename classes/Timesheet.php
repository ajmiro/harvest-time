<?php 

class Timesheet{

	private $_api,
					$_timer,
					$_data,
					$timesheetId,
					$_error;

	public function __construct( $timesheet = null ){

		if(Session::exists(Config::get('session/session_name'))) {

			require_once getcwd() . '/HaPi/Harvest/Range.php';
			require_once getcwd() . '/HaPi/Harvest/DayEntry.php';


			$this->_api = new HarvestAPI(); 
			
			$this->_api->setUser( Session::get(Config::get('session/ht_email')) ); 
			$this->_api->setPassword( Session::get(Config::get('session/ht_pass') ) ); 
			$this->_api->setAccount( Config::get('session/ht_account') );

			$this->_api->setRetryMode( HarvestAPI::RETRY );

			if( !$timesheet ){

			}else{

					$this->getEntry( $timesheet );
					$this->_timesheetId = $timesheet;
			}

		} else {

			Redirect::to('login');

		}

	}

	private function getEntry( $timesheet ){
 
		$result = $this->_api->getEntry( $timesheet );
		if( $result->isSuccess() ) {
		   $this->_data = $result->data;
		   return true;
		}

		return false;
	}

	public function data(){
		return $this->_data;
	}



	public function getUserTimesheet( $userId, $range, $projectId = null){
		
		$rangeObj = new Harvest_Range( $range[0], $range[1] );

		$result = $this->_api->getUserEntries( $userId, $rangeObj );

		if( $result->isSuccess() ) {
		   $this->_data = $result->data;
		   return $this->_data;
		}

		return false;

	}

	/* Save entry */
	public function create( $fields = array() ){

		$entry = new Harvest_DayEntry();
		foreach( $fields as $key => $value ){
			$entry->set( "{$key}", $value );
		}

		$result = $this->_api->createEntry( $entry );
		if( $result->isSuccess() ) {
		   $this->_timer = $result->data;
		   return true;
		}else{
			$this->_error = $result->data;
			return $this->_error;
		}


	}

	public function error(){
		return $this->_error;
	}

	public function delete( $timesheetId ){
		$result = $this->_api->deleteEntry( $timesheetId );
		if( $result->isSuccess() ) {
		  Session::flash('msg', '<div class="alert alert-success">Entry successfully deleted.</div>');
		}else{
			Session::flash('msg', '<div class="alert alert-danger">Error. Please try again or entry does not exists.</div>');
		}	
		Redirect::to('timesheet');

	}

	public function update( $fields ){

		echo $this->_timesheetId;

		$entry = new Harvest_DayEntry();
		$entry->set( "id", $this->_timesheetId );
		foreach( $fields as $key => $value ){
		 	$entry->set( "{$key}", $value );
		}
		 
		$result = $this->_api->updateEntry( $entry );
		if( $result->isSuccess() ) {
		  return true;
		}
		return false;

	}


}