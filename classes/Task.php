<?php 

class Task{

	private $_data,
					$_api,
					$_taskId;
	
	public function __construct( $task = null ){

		if(Session::exists(Config::get('session/session_name'))){

			$this->_api = new HarvestAPI();
			$this->_api->setUser( Session::get(Config::get('session/ht_email')) ); 
			$this->_api->setPassword( Session::get(Config::get('session/ht_pass') ) ); 
			$this->_api->setAccount( Config::get('session/ht_account') );
			$this->_api->setRetryMode( HarvestAPI::RETRY );

			if( !$task ){
				$this->getTasks();
			}else{
				$this->getTask( $task );
			}

		} else{
			Redirect::to('login');
		}
		
	}

	private function getTasks(){
		$result = $this->_api->getTasks();
		if( $result->isSuccess() ){
			$this->_data = $result->data;
			return true;
		}
		return false;
	}

	private function getTask( $task = null ){
		$result = $this->_api->getTask( $task );
	 	if( $result->isSuccess() ) {
	 		$this->_data = $result->data;
	 		return true;
	 	}

	 	return false;
	}
	
	public function data(){
		return $this->_data;
	} 

}