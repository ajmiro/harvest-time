<?php 
	$SITEURL = getcwd();
	require_once( $SITEURL . '/core/Init.php' );
	require_once( $SITEURL . '/includes/header.php' ); 

	$user = new User();
	$user->logout();

	require_once( $SITEURL . '/includes/footer.php' ); 

?>