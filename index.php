<?php 

$SITEURL = getcwd();
require_once( $SITEURL . '/core/Init.php' );
require_once( $SITEURL . '/includes/header.php' ); 

$currentUser = Config::get('session/session_name'); 

if( Session::exists($currentUser)) : 

	$userId = Session::get( $currentUser );
	$userObj = new User( $userId );
	$userDetails = $userObj->data();

	if(Input::exists('get')) {

		$action = Input::get('action');

		if( $action == 'resetPassword'){

			$userObj->resetPassword();

		} else {

			require_once( $SITEURL . '/includes/user/' . $action . '.php' ); 

		}

		
	} else {

		require_once( $SITEURL . '/includes/user/dashboard.php' ); 
	}

?>

	

<?php else: ?>

<p>Please <a href="login">log in</a>.</p>

<?php endif; ?>

<?php require_once( $SITEURL . '/includes/footer.php' )?>



