<?php 

$SITEURL = getcwd();
require_once( $SITEURL . '/core/Init.php' );
require_once( $SITEURL . '/includes/header.php' ); 

if(Session::exists(Config::get('session/session_name'))){

	$userId = Session::get(Config::get('session/session_name'));

	if( Input::get('timesheetId') ){
		
		$timesheetId = Input::get('timesheetId');
		$timesheetObj = new Timesheet($timesheetId);

		require_once( $SITEURL . '/includes/timesheet/' . Input::get('action') . '.php');

	}else if(Input::get('action')){

		$timesheetObj = new Timesheet();
		require_once( $SITEURL . '/includes/timesheet/' . Input::get('action') . '.php');

	}else {

		$timesheetObj = new Timesheet();
		require_once( $SITEURL . '/includes/timesheet/list.php');

	}

}

require_once( $SITEURL . '/includes/footer.php' ); 

?>
