// Note: See http://blog.garstasio.com/you-dont-need-jquery/ and http://youmightnotneedjquery.com/ for JS commands that don't require jQuery
$(document).ready(function(){
	$('.ht-table table').DataTable();

  var dateFormat = "yymmdd",
      from = $( "#from" ).datepicker({
          showWeek: true,
			    firstDay: 1,
			    altFormat: "yymmdd"
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to" ).datepicker({
      		showWeek: true,
	        firstDay: 1,
			    altFormat: "yymmdd"
	      })
	      .on( "change", function() {
	        from.datepicker( "option", "maxDate", getDate( this ) );
	      });

      startsOn = $( "#startOn" ).datepicker({
          showWeek: true,
          firstDay: 1,
          altFormat: "yymmdd"
        })
        .on( "change", function() {
          Endson.datepicker( "option", "minDate", getDate( this ) );
        }),
      Endson = $( "#endsOn" ).datepicker({
          showWeek: true,
          firstDay: 1,
          altFormat: "yymmdd"
        })
        .on( "change", function() {
          startsOn.datepicker( "option", "maxDate", getDate( this ) );
        });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }

    jQuery('.project-select').hide();
    jQuery('.project-select').eq(0).show();

    $('#project_id').change(function () {
      $( "select option:selected" ).each(function() {
        jQuery('#project-' + $(this).val() ).show().siblings('.project-select').hide();
    
      });
    }).change();


});
