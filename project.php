<?php 

$SITEURL = getcwd();
require_once( $SITEURL . '/core/Init.php' );
require_once( $SITEURL . '/includes/header.php' ); 

if(Session::exists(Config::get('session/session_name'))){

	if( isset($_GET['projectId'] ) ){

		
		
		$projectId = $_GET['projectId'];

		$project = new Project($projectId);

		require_once( $SITEURL . '/includes/project/' . $_GET['action'] . '.php');

	}else if( isset($_GET['action'])){

		$project = new Project();
		require_once( $SITEURL . '/includes/project/' . $_GET['action'] . '.php');

	}else {

		$project = new Project();
		require_once( $SITEURL . '/includes/project/list.php');

	}

}

require_once( $SITEURL . '/includes/footer.php' ); 

?>
