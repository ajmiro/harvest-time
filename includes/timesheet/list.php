<h1>Timesheet</h1>
<a href="timesheet?action=add" class="btn btn-link">Add New</a>
<?php 
	if(Session::exists('msg')){
		echo Session::flash('msg');
	}

	$today = date('Ymd');
	$range = array( $today, $today );

	if(Input::exists()){

		$range = array( date('Ymd', strtotime(Input::get('from'))), date('Ymd', strtotime(Input::get('to')))); 
		
	}

	$results = $timesheetObj->getUserTimesheet( $userId, $range );
	//echo '<pre>'; print_r( $results ); echo '</pre>';

?>
	
<div class="calendar-timesheet">
<hr>
<form method="post">
	from: <input type="text" name="from" id="from" required="" value="<?php echo (Input::exists()) ? Input::get('from') : date('d-m-Y') ?>" /> - 
	to: <input type="text" name="to" id="to" required="" value="<?php echo (Input::exists()) ? Input::get('to') : date('d-m-Y') ?>" />
	<button class="btn btn-primary">View Timesheet</button>
</form>
	<hr>
</div>

<?php 
	if($results) : 
?>
	<div id="timesheet-list" class="ht-table">
		<table class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<td>Project Name</td>
					<td>Hours</td>
					<td>Date</td>
					<td>Status</td>
					<td>&nbsp;</td>
				</tr>
			</thead>
			<tbody>

<?php foreach( $results as $timesheet ) : ?>
<?php 

				$timesheetId = $timesheet->id;
				
				$deleteUrl = "timesheet?timesheetId=$timesheetId&action=delete";
				$updateUrl = "timesheet?timesheetId=$timesheetId&action=update";

				$project = new Project( $timesheet->{'project-id'});
				$projectDetails = $project->get();

				$task = new Task( $timesheet->{'task-id'});
				$taskDetails = $task->data();

?>
				<tr>
					<td>
						<?php echo strtoupper($projectDetails->name) ?><br>
						<?php echo $taskDetails->name ?><br>
						<small><?php echo $timesheet->notes ?></small>

					</td>
					<td><?php echo $timesheet->hours ?></td>
					<td><?php echo date('d-m-Y', strtotime( $timesheet->{'spent-at'})) ?></td>
					<td><?php echo ( $timesheet->{'is-closed'} === 'true' ) ? 'Approved' : 'Pending' ?></td>
					<td>
						<?php if( $timesheet->{'is-closed'} == 'false' ) : ?>
						<a href="<?php echo $deleteUrl; ?>" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a> 
						| <a href="<?php echo $updateUrl; ?>">Update</a>
						<?php endif; ?>
						</td>
				</tr>

<?php endforeach; ?>
			</tbody>
		</table>
	</div>
<?php else: ?>
	<?php echo '<div class="alert alert-info text-center">No entry for today.</div>' ?>
<?php endif; ?>
