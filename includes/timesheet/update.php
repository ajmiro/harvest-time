<?php
	
	$timesheetDetails = $timesheetObj->data();

	if( $timesheetDetails->{'is-closed'} == 'true') {
		Session::flash('msg', '<div class="alert alert-warning">Cannot update approved entry.</div>');
		Redirect::to('timesheet');
	}

	if( Input::exists()){
		if( Token::check( Input::get('token') )){

			$result = $timesheetObj->update( array(
					'notes' => Input::get('notes'),
					'hours' => Input::get('hours'),
					'project_id' => $timesheetDetails->{'project-id'},
					'task_id' => $timesheetDetails->{'task-id'},
					'spent_at' => date("D, M j Y")
					//'spent_at' => "Sun, Jul 24 2016"
					
				));

				//var_dump( $result );

				if( $result ){
					Session::flash('msg', '<div class="alert alert-success">Successfully save entry.</div>');
					Input::resetInput();
					Redirect::to('timesheet');

				} else {
					$alert = Session::flash('warning', '<div class="alert alert-warning">Please check your information and try again.</div>');
				}			
		}
	}


?>

<h1>Update Entry</h1>

<div id='timesheet-create' class='ht-form'>
	<form method="POST">	
		<p>
			<label>Time spent <small>(1.5 is equivalent to 1:30, right now use 1.5)</small></label>
			<input type="number" name="hours" min='0'>
		</p>

		<p>
			<label>Notes</label>
			<textarea name="notes"></textarea>
		</p>

			<input type='hidden' name='token' value='<?php echo Token::generate(); ?>' />
		<p>
			<button class="btn btn-primary">Save Entry</button>
		</p>
	</form><!-- end form -->
</div><!-- end ht-form -->