<?php
	
	$projectsObj = new Project();
	$projects = $projectsObj->getProjects();


	if( Input::exists()){
		if( Token::check( Input::get('token') )){

			$result = $timesheetObj->create( array(
					'notes' => Input::get('notes'),
					'hours' => Input::get('hours'),
					'project_id' => Input::get('project_id'),
					'task_id' => Input::get('project_' . Input::get('project_id')),
					'spent_at' => date("D, M j Y")
					
				));

				if( $result ){
					Session::flash('msg', '<div class="alert alert-success">Successfully save entry.</div>');
					Input::resetInput();
					Redirect::to('timesheet');

				} else {
					Session::flash('warning', '<div class="alert alert-warning"> ' .  $result . ' </div>');
				}			
		}
	}


?>

<h1>New Entry</h1>

<div id='timesheet-create' class='ht-form'>
	<form method="POST">
<?php if( $projects ): ?>
		<p>
			<label>Select Project</label>
			<select name="project_id" id="project_id">
<?php foreach( $projects as $project ): ?>
				<option value="<?php echo $project->id ?>"><?php echo $project->name ?></option>
<?php endforeach; ?>			
			</select>
			<br>
		</p>
<?php foreach( $projects as $project ): ?>
<?php   $projectId = $project->id;  
				$taskAssignment = $projectsObj->getProjectTaskAssignments( $projectId );

				if( !empty( $taskAssignment )) : 
?>
		<p id="project-<?php echo $projectId ?>" class="project-select">

			<label>Tasks</label>
			<select name="project_<?php echo $projectId ?>">
			<?php foreach( $taskAssignment as $assignment ): ?>

				<?php 
							$itemObj = new Task( $assignment->{'task-id'} ); 
							$itemDetails = $itemObj->data();
				?>

							<option value="<?php echo $assignment->{'task-id'} ?>"><?php echo $itemDetails->name ?></option>

			<?php endforeach;  ?>			
			</select>

		</p>

			<?php endif; ?>

<?php endforeach; ?>

<?php endif; ?>
		<p>
			<label>Time spent <small>(1.5 is equivalent to 1:30, right now use 1.5)</small></label>
			<input type="number" name="hours" min='0'>
		</p>

		<p>
			<label>Notes</label>
			<textarea name="notes"></textarea>
		</p>

			<input type='hidden' name='token' value='<?php echo Token::generate(); ?>' />
		<p>
			<button class="btn btn-primary">Save Entry</button>
		</p>
	</form><!-- end form -->
</div><!-- end ht-form -->