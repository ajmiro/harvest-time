<?php
	$alert = '';
	$clientsObj = new Client();
	$clients = $clientsObj->getClients();

	$tasksObj = new Task();
	$tasks = $tasksObj->data();

	$userObj = new User();
	$members = $userObj->getUsers();

	//echo '<pre>'; print_r( $members ); echo '</pre>';

	if( Input::exists() ){
		
		if( Token::check( Input::get('token') )){

				$result = $project->create( array(
					'active' => true,
					'active-task-assignments-count' => count(Input::get('tasks')),
					'client-id' => Input::get('client_id'),
					'code' => Input::get('code'),
					'name' => Input::get('name'),
					'notes' => Input::get('notes'),
					'starts-on' => date('Y-m-d' , strtotime( Input::get('starts_on') )),
					'ends-on' => date('Y-m-d' , strtotime( Input::get('ends_on') )),
					'active-user-assignments-count' => 1, //currently set to one for only the owner who creates it becomes a default team member
					'tasks' => Input::get('tasks')
					// 'billable' => (Input::get('billable')) ? 'true' : 'false',
					// 'bill-by' => Input::get('billable_by')
					// 'budget' => Input::get('budget'),
					// 'budget-by' => Input::get('budget_by'),
					// 'cost-budget' => Input::get('cost_budget'),
					// 'notify-when-over-budget' => Input::get('notify_when_over_budget'), 
					// 'over-budget-notification-percentage' => Input::get('over_budget_notification_percentage')
					// 'basecamp-id' => '',
					// 'bill-by' => Input::get('billable_by'),
					// 'cache-version' => '',
					// 'cost-budget-include-expenses' => false,
					// 'fees' => '',
					// 'hourly-rate' => '',
					// 'over-budget-notified-at' => '',
					// 'show-budget-to-all' => false,
					// 'estimate' => '',
					// 'estimate-by' => '',
					
				));



				if( $result ){
					Session::flash('msg', '<div class="alert alert-success">Successfully save project.</div>');
					Input::resetInput();
					Redirect::to('project');

				} else {
					$alert = Session::flash('warning', '<div class="alert alert-warning">Please check your information and try again.</div>');
				}			
		}
	}


?>

<div class="breadcrubms">
		<a href="index">Dashboard</a> &rsaquo; 
		<a href="project">Project</a> &rsaquo;
		<span class="active-page">New Project</span>
</div>
<h1>New Project</h1>

<div id='project-create' class='ht-form'>
	<?php echo $alert; ?>
	<form method="POST">
<?php if( $clients ): ?>
		<p>
			<label for='client'>Client</label>
			<select name="client_id" required>
<?php foreach( $clients as $client ): ?>
				<option  value="<?php echo $client->id ?>"><?php echo $client->name ?></option>
<?php endforeach; ?>
			</select>
		</p>
		<hr>
<?php endif; ?>
		<p>
			<label for='name'>Name</label>
			<input type='text' class='input' name='name' required value="<?php echo (Input::exists()) ? Input::get('name') : ''  ?>" />
		</p>
		<hr>
		<p>
			<label for='code'>Code</label>
			<input type='text' class='input' name='code' />
		</p>
		<hr>
		<p>
			<label for=''>Dates</label>
			<span>Starts on<span>
			<input type='date' class='input' name='starts_on' min="<?php echo date('Y-m-d') ?>" /><br>
			<span>Ends on<span>
			<input type='date' class='input' name='ends_on'  min="<?php echo date('Y-m-d') ?>" />
		</p>
		<hr>

<?php if( $tasks ): ?>
		<br>
		<p>
			<label for="tasks">Tasks</label>
<?php foreach( $tasks as $task ): ?>
			<input type="checkbox" name="tasks[]" checked="checked" value="<?php echo $task->id ?>"> <?php echo $task->name; ?><br>
<?php endforeach; ?>
		</p>
		<hr>
<?php endif; ?>

<?php if( $members ): ?>
		<p>
			<label for="members">Members</label>
<?php foreach( $members as $member ) : ?>
			<input type="checkbox" name="members[]" value=<?php echo $member->id ?>> <?php echo $member->name; ?><br>
<?php endforeach; ?>		
		</p>
		<hr>
<?php endif; ?>
		<p>
			<label for='notes'>Notes</label>
			<textarea name="notes"></textarea>
		</p>
			<input type='hidden' name='token' value='<?php echo Token::generate(); ?>' />
		<p>
			<button class="btn btn-primary">Save Project</button>
		</p>
	</form><!-- end form -->
</div><!-- end ht-form -->


