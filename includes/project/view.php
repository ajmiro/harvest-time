<?php 

	$projectDetails = $project->get();

	$projectName = $projectDetails->name;
	$projectCode = $projectDetails->code;
	$projectBudget = $projectDetails->budget;
	$projectActive = ( $projectDetails->active ) ? 'Active' : 'Inactive';
	$projectCreated = $projectDetails->{'created-at'};
	$projectStart = $projectDetails->{'starts-on'};
	$projectEnd = $projectDetails->{'ends-on'};
	$projectNotes = $projectDetails->notes;
	$projectClient = $projectDetails->{'client-id'};

	//get Project Client 
	$client = new Client( $projectClient );
	$clientDetails = $client->get();

	//get Tasks 
	$tasks = $project->getTasks();
	$teamMembers = $project->getTeamMembers();

?>

<div class="row group">
	<div class="span12">
		<h1><?php echo $projectName ?></h1>
	</div>
	<div class="span12">
		<hr>
	Actions: 
	<a href="project?productId<?php echo $projectId ?>&action=delete" class="btn btn-link" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a> | 
	<a href="project?productId<?php echo $projectId ?>&action=update" class="btn btn-link">Update</a>
	<hr>
	</div>

	<div class="span2">Code</div>
	<div class="span9"><?php echo $projectCode ?></div>

	<div class="span2">Client</div>
	<div class="span9"><?php echo $clientDetails->name ?></div>

	<div class="span2">Budget</div>
	<div class="span9"><?php echo number_format(intval($projectBudget), 2 ) ?></div>

	<div class="span2">Status</div>
	<div class="span9"><?php echo $projectActive ?></div>

	<div class="span2">Created on</div>
	<div class="span9"><?php echo $projectCreated ?></div>

	<div class="span2">Starts on</div>
	<div class="span9"><?php echo $projectStart ?></div>	

	<div class="span2">Ends on</div>
	<div class="span9"><?php echo $projectEnd ?></div>

</div>
<?php if( $tasks ) : ?>
<hr>
<div class="row group">
	<div class="span12">
		<h2>Tasks</h2>
		<table>
			<thead>
				<tr>
					<td>Billable Tasks</td>
				</tr>
			</thead>
			<tbody>
<?php foreach( $tasks as $task ) : ?>
<?php 
				$taskObj = new Task( $task->{'task-id'} );
				$taskDetails = $taskObj->data();
?>
				<tr>
					<td><?php echo $taskDetails->name ?></td>
				</tr>
<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td>Total</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
<?php endif; //end tasks ?>
<?php if( $teamMembers ) : ?>
<hr>
<div class="row group">
	<div class="span12">
		<h2>Team</h2>
		<table>
			<thead>
				<tr>
					<td>Team Members</td>
				</tr>
			</thead>
			<tbody>
<?php foreach( $teamMembers as $teamMember ) :  ?>
<?php 

				$member = new User( $teamMember->{'user-id'} );
				$teamMemberDetails = $member->get( array('first-name', 'last-name'));
				//echo '<pre>'; print_r( $teamMemberDetails ); echo '</pre>';

?>
				<tr>
					<td><?php echo $teamMemberDetails["'first-name'"] . ' ' . $teamMemberDetails["'last-name'"] ?></td>
				</tr>
<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif; ?>



