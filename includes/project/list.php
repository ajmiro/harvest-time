<h1>Projects</h1>
<a href="project?action=add" class="btn btn-link">Add New</a>
<?php 

	if(Session::exists('msg')){
		echo Session::flash('msg');
	}

	$results = $project->getProjects();

	if($results) : 

?>
	<div id="projects-list" class="ht-table">
		<table class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<td>Name</td>
					<td>Budget</td>
					<td>Status</td>
					<td>Starts On</td>
					<td>Ends On</td>
					<td>&nbsp;</td>
				</tr>
			</thead>
			<tbody>

<?php foreach( $results as $project ) : ?>
<?php 

				$projectId = $project->id;
				
				$viewUrl = "project?projectId=$projectId&action=view";
				$deleteUrl = "project?projectId=$projectId&action=delete";
				$updateUrl = "project?projectId=$projectId&action=update";

				$token = Token::generate();

?>
				<tr>
					<td>
						<a href="<?php echo $viewUrl; ?>"><?php echo $project->name; ?></a>
					</td>
					<td><?php echo number_format(intval($project->budget), 2 ); ?></td>
					<td><?php echo ($project->active) ? 'Active' : 'Inactive'; ?></td>
					<td><?php echo $project->{'starts-on'}; ?></td>
					<td><?php echo $project->{'ends-on'}; ?></td>
					<td>
						
						<a href="<?php echo $viewUrl; ?>">View</a> | 
						<a href="<?php echo $deleteUrl; ?>" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a> | 
						<a href="<?php echo $updateUrl; ?>">Update</a></td>
				</tr>

<?php endforeach; ?>
			</tbody>
		</table>
	</div>

<?php endif; ?>