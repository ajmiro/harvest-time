<h1>Dashboard</h1>
<?php 
	if(Session::exists('msg')){
		echo Session::flash('msg');
	}
?>
<div class="span8">
	<div class="alert alert-empty">
		<p>Welcome <?php echo $userDetails->{'first-name'} ?>!
		<br><small><?php echo ($userDetails->{'is-admin'}) ? 'Admin Account' : '' ?></small>
		</p>
		
		<a href="?action=update">Update Profile</a>
	</div>

	<h2>Account Details</h2>
</div>
<div class="span4">
	<h3>Projects</h3>
	<p><a href="project?action=add" class="btn btn-link">Add New</a> | <a href="project" class="btn btn-link">View All</a></p>
	<p></p>

	<h3>Timelines</h3>
	<p><a href="timesheet?action=add" class="btn btn-link">Add New</a> | <a href="timesheet" class="btn btn-link">View All</a></p>

	<!-- <h3>Users</h3>
	<p><a href="user?action=add" class="btn btn-link">Add New</a> | <a href="user" class="btn btn-link">View All</a></p> -->
</div>