<?php 
	$billableRate = $userDetails->{'default-hourly-rate'};

	if(Input::exists() ) {

		$userObj->update( array(
				'first-name' => Input::get('first_name'),
				'last-name' => Input::get('last_name'),
				'department' => Input::get('department'),
				'billable-rate' => Input::get('billable_rate')
			));

	}
?>

<h1>Update Profile</h1>
<?php 
	if(Session::exists('msg')){
		echo Session::flash('msg');
	}
?>
<form method="post">
	<p>
		<label>First Name</label>
		<input type="text" name="first_name" required value="<?php echo $userDetails->{'first-name'} ?>">
	</p>
	<p>
		<label>Last Name</label>
		<input type="text" name="last_name" required value="<?php echo $userDetails->{'last-name'} ?>">
	</p>
	<p>
		<label>Email</label>
		<input type="email" name="last_name" disabled value="<?php echo $userDetails->email ?>">
	</p>
	<p>
		<label>Department</label>
		<input type="text" name="department" value="<?php echo $userDetails->department ?>">
	</p>
	<p>
		<label>Billable Rate</label>
		<input type="number" name="billable_rate" min="0" value="<?php echo $billableRate ?>">
	</p>
	<p>
			<button class="btn btn-primary">Save</button>
		</p>

</form>