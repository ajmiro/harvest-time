<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Harvest API </title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="css/main.css">
	<!--[if lt IE 9]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
</head>
<body>

<header role="banner">
		<div class="container">
			<nav role="navigation">
				<a href="index">Home</a>
			<?php if(Session::exists(Config::get('session/session_name'))) : ?>
				<a href="timesheet">Timesheets</a>
				<a href="project">Projects</a>
				<a href="logout">Logout</a>
			<?php else: ?>
				<a href="login">Login</a>
			<?php endif;?>
			</nav>
		</div>
	</header> 
	<main role="main">
		<div class="container">