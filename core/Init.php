<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR); 

session_start();

$GLOBALS['config'] = array(
	'remember' => array(
		'cookie_name' => 'hash',
		'cookie_expiry' => 604800
	),
	'session' => array(
		'session_name' => 'user',
		'token_name' => 'token',
		'ht_email' => 'email',
		'ht_pass' => 'pass',
		'ht_account' => 'metafusion'
	),
	'site_dir' => getcwd()
);

require_once getcwd() . '/HaPi/HarvestAPI.php';

spl_autoload_register( array('HarvestAPI', 'autoload') );
spl_autoload_register( function( $class ) {
	require_once 'classes/' . $class . '.php';
});

require_once 'functions/sanitize.php';

