<?php 

require_once( dirname(__FILE__) . '/core/Init.php' );

$alert = '';

if( isset( $_POST['username'] ) && isset( $_POST['password'] ) ){

	if( Token::check( $_POST['token'] )){

		$user = new User();
		$result = $user->login( $_POST['username'], $_POST['password'] );

		if( !$result ){
			$alert = Session::flash('warning', '<div class="alert alert-warning">Incorrect email and/or password. Please try again</div>');
		} else {
			//echo Session::get(Config::get('session/session_name'));
			Redirect::to('index');
		}
	}

} 



?>

<?php require_once( Config::get('site_dir') . '/includes/header.php' ); ?>
<h1>Login</h1>

<div id='user-login' class='ht-form'>
	<?php echo $alert; ?>
	<form method="POST">
		<p>
			<label for='username'>Email</label>
			<input type='email' class='input' name='username' required />
		</p>
		<p>
			<label for='password'>Password</label>
			<input type='password' class='input' name='password' required />
		</p>
			<input type='hidden' name='token' value='<?php echo Token::generate(); ?>' />
		<p>
			<button class="btn btn-primary">Log in </button>
		</p>
		
	</form><!-- end form -->
</div><!-- end ht-form -->


<?php require_once( Config::get('site_dir') . '/includes/footer.php' )?>

